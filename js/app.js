(function() {

  var app = angular.module('app', ['angular-svg-round-progress']);

  var MainController = function($scope, roundProgressService) {

    $scope.current = 60;
    $scope.max = 100;
    $scope.timerCurrent = 0;
    $scope.uploadCurrent = 0;
    $scope.stroke = 20;
    $scope.radius = 125;
    $scope.isSemi = false;
    $scope.rounded = true;
    $scope.responsive = true;
    $scope.clockwise = true;
    $scope.currentColor = '#5da4d9';
    $scope.bgColor = '#eaeaea';
    $scope.duration = 800;
    $scope.currentAnimation = 'easeOutCubic';

    $scope.animations = [];

    angular.forEach(roundProgressService.animations, function(value, key) {
      $scope.animations.push(key);
    });

    $scope.getStyle = function() {
      var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

      return {
        'top': $scope.isSemi ? 'auto' : '50%',
        'bottom': $scope.isSemi ? '5%' : 'auto',
        'left': '50%',
        'transform': transform,
        '-moz-transform': transform,
        '-webkit-transform': transform,
        'font-size': $scope.radius / 3.5 + 'px'
      };
    };

    $scope.getColor = function() {
      return $scope.currentColor;
    };

    var getPadded = function(val) {
      return val < 10 ? ('0' + val) : val;
    };

  };

  app.controller('MainController', ['$scope', 'roundProgressService', MainController]);
}());
